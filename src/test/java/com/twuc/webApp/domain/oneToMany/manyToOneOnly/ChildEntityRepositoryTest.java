package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.omg.CORBA.DynAnyPackage.Invalid;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> child = new ClosureValue<>();
        ClosureValue<Long> parent = new ClosureValue<>();

        flushAndClear(em -> {
            ChildEntity childEntity = new ChildEntity("child");
            ChildEntity saveChild = childEntityRepository.save(childEntity);
            child.setValue(saveChild.getId());

            ParentEntity parentEntity = new ParentEntity("parent");
            childEntity.setParentEntity(parentEntity);
            ParentEntity savePatent = parentEntityRepository.save(parentEntity);
            parent.setValue(savePatent.getId());
        });

        run(em -> {
            ChildEntity childEntity = childEntityRepository.findById(child.getValue()).orElseThrow(RuntimeException::new);
            assertNotNull(childEntity);
            assertEquals("parent", childEntity.getParentEntity().getName());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> child = new ClosureValue<>();
        ClosureValue<Long> parent = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parentEntity = new ParentEntity("parent");
            ParentEntity saveParent = parentEntityRepository.save(parentEntity);
            parent.setValue(saveParent.getId());

            ChildEntity childEntity = new ChildEntity("child");
            childEntity.setParentEntity(parentEntity);
            ChildEntity saveChild = childEntityRepository.save(childEntity);
            child.setValue(saveChild.getId());
        });

        flushAndClear(em -> {
            ChildEntity childEntity = childEntityRepository.findById(child.getValue()).orElseThrow(RuntimeException::new);
            childEntity.setParentEntity(null);
        });

        run(em -> {
            ChildEntity childEntity = childEntityRepository.findById(child.getValue()).orElseThrow(RuntimeException::new);
            assertNull(childEntity.getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> child = new ClosureValue<>();
        ClosureValue<Long> parent = new ClosureValue<>();


        flushAndClear(em -> {
            ParentEntity parentEntity = new ParentEntity("parent");
            ParentEntity saveParent = parentEntityRepository.save(parentEntity);
            parent.setValue(saveParent.getId());

            ChildEntity childEntity = new ChildEntity("child");
            childEntity.setParentEntity(parentEntity);
            ChildEntity savedChild = childEntityRepository.save(childEntity);
            child.setValue(savedChild.getId());
        });


        flushAndClear(em -> {
            childEntityRepository.deleteById(child.getValue());
            parentEntityRepository.deleteById(parent.getValue());
        });

        
        run(em -> {
            assertEquals(0, childEntityRepository.findAll().size());
            assertEquals(0, parentEntityRepository.findAll().size());
        });
        // TODO: 2019/10/10  

        // --end-->
    }
}
